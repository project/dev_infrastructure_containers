#!/bin/bash

export LOCAL_IP=$(hostname -I)
env
# Create a basic mysql install
if [ ! -d /var/lib/mysql/mysql ]; then
  echo "******* Creating a bare mysql install *******"
  /usr/bin/mysql_install_db > /dev/null
  /usr/bin/mysqld_safe --skip-syslog &
  cd /var/lib/mysql
  echo ${DBPASS} > /var/lib/mysql/mysql/mysql-pw.txt
  mysqladmin --silent --wait=30 ping || exit 1 && \
  mysql -ve "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;" && \
  mysql -u root -ve "CREATE DATABASE ${DBNAME};" && \
  mysql -ve "GRANT ALL PRIVILEGES ON ${DBNAME}.* TO '${DBNAME}'@'%' IDENTIFIED BY '${DBPASS}';" && \
  bzcat -v ${DBNAME}_database_snapshot.dev-current.sql.bz2 | \
  mysql -uroot  ${DBNAME} && \
  killall mysqld
else
  echo
  echo "--------------------------STARTING SERVICES-----------------------------------"
  echo "USE CTRL+C TO STOP THIS APP"
  echo "------------------------------------------------------------------------------"
  supervisord -n -c /etc/supervisor/conf.d/supervisord.conf
fi