#!/bin/bash -e
############################################
# Use this script to recreate all containers
# Contrib by Ricardo Amaro 2015
############################################

DBUSER=root
DBPASS=drupal
BASEDIR=$HOME

### Change only above this line ####

MAINDIR=$(pwd)
echo "** START: $(date) **"
cd mariadb
sudo time docker build -t devwww/mariadb .
cd ${MAINDIR}

for DB in $(ls -d db*) ; do
  DBNAME=$(echo ${DB} | awk -F'_' '{print $2}')
  echo "$(basename ${DB})";
  cd ${MAINDIR}/${DB};
  echo "Building: ${DB}";
  
  #CREATE MYSQL DIR
  sudo rm -rf ${BASEDIR}/db/${DBNAME}
  sudo mkdir -p ${BASEDIR}/db/${DBNAME}
 
  #REMOVE PREVIOUS BUILD
  sudo docker stop ${DB}  2>/dev/null | true
  sudo docker rm -f ${DB} 2>/dev/null | true

  #COPY THE DUMP AND START IMPORT
  sudo cp -v ${DBNAME}_database_snapshot.dev-current.sql.bz2 ${BASEDIR}/db/${DBNAME}/.
  sudo time -p docker run -e DBPASS=${DBPASS} -e DBNAME=${DBNAME} \
  --volume=${BASEDIR}/db/${DBNAME}:/var/lib/mysql devwww/mariadb

  sudo rm ${BASEDIR}/db/${DBNAME}/${DBNAME}_database_snapshot.dev-current.sql.bz2

  #COPY THE INNODB FILES INTO PLACE
  sudo time -p docker run -e DBPASS=${DBPASS} -e DBNAME=${DBNAME} \
  --name ${DB} --volume=${BASEDIR}/db/${DBNAME}:/var/lib/tmp \
  devwww/mariadb /bin/bash -c "/usr/bin/rsync -avr --delete /var/lib/tmp/ /var/lib/mysql/"

  #SAVE THE IMAGE FOR USAGE
  sudo docker commit -m="${DB}-$(date)" ${DB} devwww/${DB}
done

cd ${MAINDIR}
echo "** END: $(date) **"
