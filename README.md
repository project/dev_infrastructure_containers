devdrupal databases
===================

This repo contains the devdrupal databases

# Instructions:

## 1 - Preparation
Add the respective ${DBNAME}_database_snapshot.dev-current.sql.bz2
to every db_* folder.

DBUSER and the DBPASS can be update on the 2 lines of build.sh.

## 2 - Build

To build all the containers, run:

```
./build.sh
```

## 3 - Run
To run a db container you can add to jenkins.
Example:

```
docker run -it --name ${DEVWWW} -p ${HOSTPORT}:3306 devwww/db_${DBNAME} /start.sh
```
Port 3306 can be mapped to other HOSTPORT on the host.

DEVWWW is dev environment unique name
HOSTPORT is the mysql seen port on the host
DBNAME is api/drupal/association...

# Docker installation docs:
https://docs.docker.com/installation/

# NOTES

## Contributing
Feel free to submit any bugs, requests, or fork and contribute
to this code. :)

1. Fork the repo
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Authors

Created and maintained by [Ricardo Amaro][author]
http://blog.ricardoamaro.com
